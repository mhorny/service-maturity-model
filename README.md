# Service Maturity Model

A model for measuring the maturity of a service.

## Purpose

Services maintained by IT organizations are at various points of maturity throughout their lifecycle. Some of these services are put into production prematurely by infrastructure engineering teams, some neglected and degrade over time, and some are inherited by operations teams from development teams without much talk of production-readiness.

These brownfield (legacy) deployments can incur the operations team significant technical debt that needs to be prioritized and fixed over time. One approach for prioritizing this work is to assess these deployments and determine their state in order to decide what, (if anything), needs to be added to the team's backlog.

This service maturity model is that yardstick for Dreamer Labs. Engineering teams use this to determine the state of both greenfield (new) and brownfield (legacy) services, to help us to identify our worst technical debt and groom our backlogs accordingly.

This model is best suited for smaller organizations where the engineering team that initially develops a new service is responsible for providing ITIL-defined tier 0-3 support for the service until tier 1-2 and most tier 0 support can be handed-off to an operations team to manage the daily maintenance and support for the service.

## Versioning

Each version of this model will use a semantic versioning to indicate changes when compared to the previous version.
Each service definition file should reference the version number of the model it implements.

## Definitions

**Service**
A collection of software and infrastructure/environment that is designed to solve one or more business problems for an enterprise.
Services are built and/or deployed by Product Teams.
Services are used by Service Consumers.
Services are provided by Service Providers.

**Product Team**
The software development and/or infrastructure/environment engineering team designated to bulid a Service.

**Product Owner**
The customer representative that controls the contents of the Product Team's backlog and sprints.

**Service Consumer**
The entity that will interface with the Service.

**Service Provider**
The entity that will manage a service built by a Product Team and used by a Service Consumer.

**Stakeholders**
Any entity that has influence over the contents of the Product Team's backlog.
These entities commonly influence things like design constraints of a new Service.
These entities include, (but are not limited to), Service Providers, Service Consumers, and the Service's Product Owner.

## Maturity Levels

Service maturity levels in this model align with the typical lifecycle of services. Each section below consists of a general description of the maturity level and the attributes a service must possess to met the minimum criterion for that maturity level. Attributes are each assigned an attribute ID (AID) to allow for quick reference.

### Not Ready (NR)

Not-ready services are still amorphous concepts or capabilites that the organization has not yet well-defined. These services have no clear problem defined that the service is supposed to solve, no target consumer, and/or no service provider identified.

### Research Ready (RR)

Research-ready services are still conceptual in nature, but are ready-for-work by an engineering team.
There is at least one clearly defined problem that the service is supposed to solve for, and a target consumer it is supposed to help.

### Demonstration Ready (DR)

Demonstration-ready services consist of one or more functional prototypes that have at least one public interface that is demoable-to and/or testable-by a service consumer. These services do not yet have the interfaces needed to be minimally supportable by the provider or minimally usable by the consumer.

It is not uncommon for a protype within a demonstration-ready service to be one of many prototypes deployed as part of a "bake-off" to determine which application stack a consumer prefers to interface with to solve a defined problem they have. As a result, minimal effort and emphasis should be placed on solving for provider-related interfaces like logging, monitoring, and backups. Not all prototypes within a demonstration-ready service are intended to eventually become production or enterprise-ready services. Some are just deployed to gather data and better refine consumer requirements and expectations. As a result, no promises should be made to consumers regarding selection of a specific stack, as a lack of supportablilty could render the application stack non-viable for selection as the basis of a production (and later, enterprise) service.

### Preview Ready (PR)

Preview-ready services meet the minimum viability criterion identified by the service consumers to make it minimally usable, and is minimally supportable by current service provider. Defining a service-level agreement (SLA) is not a requirement for preview-ready services.

### Enterprise Ready (ER)

Enterprise-ready services meet the minimum viability criteria identified by the providers to make it minimally supportable.
This usually involves integrating the service with the enterprise's central logging, monitoring, and backup services,
and then transferring a large portion of operations responsibilities to a team besides the product team.

## Using the Model

See the `example.yml` in this repository and use it as a template to define each service mantained by your team. Define one service per service definition file and either include the service's definition file in the service's codebase somewhere, or maintain a central repository acting as a service catalog containing all service definition files. If you choose the later, come up with a naming schema to ensure definition files are consistently named.